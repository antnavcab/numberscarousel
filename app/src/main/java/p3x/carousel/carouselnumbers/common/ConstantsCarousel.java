package p3x.carousel.carouselnumbers.common;

/**
 * Created by p3x on 16/01/16.
 */
public class ConstantsCarousel {

    public static final String LANG_ESP = "ESP";
    public static final String LANG_ENG = "ENG";

    public static final String PARAMETER_LANG_ACTIVITY = "LANGUAGE";
}
