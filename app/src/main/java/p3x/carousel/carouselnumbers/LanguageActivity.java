package p3x.carousel.carouselnumbers;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import p3x.carousel.carouselnumbers.common.ConstantsCarousel;

import p3x.carousel.carouselnumbers.R;

public class LanguageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    /**
     * Metodo que captura el evento onclick del boton de seleccion de lenguage
     * @param v vista actual de seleccion del lenguage
     */
    public void clickButtonLanguage(View v)
    {
        String language;
        switch (v.getId()) { //miramos que bonton ha pulsado atraves del id.
            case R.id.imgSpain:
                language = ConstantsCarousel.LANG_ESP;
                break;
            case R.id.imgEngl:
                language = ConstantsCarousel.LANG_ENG;
                break;
            default:
                language = ConstantsCarousel.LANG_ESP;
                break;

        }

        // Creamos un intent para pasar a la actividad destino y le pasamos el paramtro lenguage seleccionado
        Intent intent = new Intent();
        intent.setClass(LanguageActivity.this, MainActivity.class);
        intent.putExtra(ConstantsCarousel.PARAMETER_LANG_ACTIVITY, language);
        startActivity(intent);
    }

	public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }

    }

}
