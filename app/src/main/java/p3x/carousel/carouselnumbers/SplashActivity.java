package p3x.carousel.carouselnumbers;
// ---------------------------------------------------------------------------------------------
// Esta actividad tan solo muestra un logotipo y luego tras esperar unos segundos carga otra
// actividad.
// Version 1.0: serranodani --> funciona que no es poco

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import p3x.carousel.carouselnumbers.R;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Definición de variables de este método
        // tiempo_espera: el tiempo (en segundos) que queremos esperar antes de pasar
        // a la actividad destino
        int tiempo_espera = 2;
        // T es un objeto de la clase Timer que se usa para hacer la pausa
        Timer T = new Timer();

        // Arrancamos la actividad y
        // ponemos el layout (que tan solo muestra un logo centrado)
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Programamos una actividad a realizar dentro de tiempo_espera segundos
        // para ello usamos el objeto T (primer parámetro la tarea a realizar y segundo
        // tiempo en el que queremos que se ejecute)
        T.schedule(new TimerTask() {
            @Override
            public void run() {
                // Creamos un intent para pasar a la actividad destino
                Intent intent = new Intent();
                // definimos las propiedades del intent
                intent.setClass(SplashActivity.this, LanguageActivity.class);
                // lo ejecutamos para pasar a la actividad destino
                startActivity(intent);
                // llamando al método finish la actividad origen es eliminada,
                // así cuando el usuario está en la actividad destino si pulsa el botón
                // atras no volverá al splash, sino saldrá de la aplicación
                finish();
            }
        }, tiempo_espera * 1000);

    }
}
