package p3x.carousel.carouselnumbers;
// ---------------------------------------------------------------------------------------------
// Esta actividad hace al carrusel de imagenes y sonidos.

import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.view.Menu;
import android.view.MenuItem;


import p3x.carousel.carouselnumbers.R;
import p3x.carousel.carouselnumbers.common.ConstantsCarousel;

public class MainActivity extends AppCompatActivity {

    // Atributos de la clase
    // imagen --> objeto usado para acceder a la imagen que mostramos al usuario
    public ImageView imagen;
    // Listado de imagenes (hay que inicializarlo con los recursos que tengamos)
    public int[] recursosImagenes = new int[10];
    // Listado de sonidos (hay que inicializarlo con los recursos que tengamos)
    // mucho ojo, usamos el mismo índice en estos dos listados para poner una imagen y su sonido
    public int[] sonidosNumerosSpanish = new int[10];
    public int[] sonidosNumerosEnglish = new int[10];
    // puntero que usaremos para recorrer el listado de imagenes
    public int puntero = 0;

    /*Clases necesarios para aplicar efectos de sonidos*/
    String languageSelected;
    private SoundPool soundPool;
    AudioManager audioManager;
    boolean plays = false, loaded = false;
    float actVolume, maxVolume, volume;
    int idAnimalSound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Rellenamos los arrays de las imagenes y de los sonidos
        this.inicializarRecursos();

        //inicializamos los recursos array de imagenes y de sonidos
        this.inicializarRecursos();

        // asignamos un valor aleatorio al puntero
        //this.puntero = (int) (Math.random() * 8);
        imagen = (ImageView) findViewById(R.id.imagen);

        this.carouselImagesNumbers(imagen);
    }

    public void carouselImagesNumbers(View v) {

        // Enlazamos el objeto imagen a la imagen que hemos definido en el layout
        this.imagen = (ImageView) v;

        if(languageSelected.equals(ConstantsCarousel.LANG_ESP)) {
            imagen.setImageResource(recursosImagenes[puntero]);
            this.soundPool.play(sonidosNumerosSpanish[puntero], 1, 1, 0, 0, 1);
        }else {
            imagen.setImageResource(recursosImagenes[puntero]);
            this.soundPool.play(sonidosNumerosEnglish[puntero], 1, 1, 0, 0, 1);
        }

        //soundPool.play(sonidosAnimales[puntero],1,1,0,0,1);
        // preparamos el puntero para la siguiente imagen
        puntero++;
        // si nos salimos de la lista volvemos a empezar
        if (puntero > 9) puntero = 0;

    }


    /** sonidos
     * Metodo de carga de sonidos e imáenges
     */
    private void inicializarRecursos() {

        languageSelected = (String) getIntent().getExtras().getSerializable(ConstantsCarousel.PARAMETER_LANG_ACTIVITY);
        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC,0);

        this.recursosImagenes[0] = R.drawable.number0;
        this.recursosImagenes[1] = R.drawable.number1;
        this.recursosImagenes[2] = R.drawable.number2;
        this.recursosImagenes[3] = R.drawable.number3;
        this.recursosImagenes[4] = R.drawable.number4;
        this.recursosImagenes[5] = R.drawable.number5;
        this.recursosImagenes[6] = R.drawable.number6;
        this.recursosImagenes[7] = R.drawable.number7;
        this.recursosImagenes[8] = R.drawable.number8;
        this.recursosImagenes[9] = R.drawable.number9;

        this.sonidosNumerosSpanish[0] = soundPool.load(getApplicationContext(),R.raw.cero,1);
        this.sonidosNumerosSpanish[1] = soundPool.load(getApplicationContext(),R.raw.uno,1);
        this.sonidosNumerosSpanish[2] = soundPool.load(getApplicationContext(),R.raw.dos,1);
        this.sonidosNumerosSpanish[3] = soundPool.load(getApplicationContext(),R.raw.tres,1);
        this.sonidosNumerosSpanish[4] = soundPool.load(getApplicationContext(),R.raw.cuatro,1);
        this.sonidosNumerosSpanish[5] = soundPool.load(getApplicationContext(),R.raw.cinco,1);
        this.sonidosNumerosSpanish[6] = soundPool.load(getApplicationContext(),R.raw.seis,1);
        this.sonidosNumerosSpanish[7] = soundPool.load(getApplicationContext(),R.raw.siete,1);
        this.sonidosNumerosSpanish[8] = soundPool.load(getApplicationContext(),R.raw.ocho,1);
        this.sonidosNumerosSpanish[9] = soundPool.load(getApplicationContext(),R.raw.nueve,1);

        this.sonidosNumerosEnglish[0] = soundPool.load(getApplicationContext(),R.raw.zero,1);
        this.sonidosNumerosEnglish[1] = soundPool.load(getApplicationContext(),R.raw.one,1);
        this.sonidosNumerosEnglish[2] = soundPool.load(getApplicationContext(),R.raw.two,1);
        this.sonidosNumerosEnglish[3] = soundPool.load(getApplicationContext(),R.raw.three,1);
        this.sonidosNumerosEnglish[4] = soundPool.load(getApplicationContext(),R.raw.four,1);
        this.sonidosNumerosEnglish[5] = soundPool.load(getApplicationContext(),R.raw.five,1);
        this.sonidosNumerosEnglish[6] = soundPool.load(getApplicationContext(),R.raw.six,1);
        this.sonidosNumerosEnglish[7] = soundPool.load(getApplicationContext(),R.raw.seven,1);
        this.sonidosNumerosEnglish[8] = soundPool.load(getApplicationContext(),R.raw.eight,1);
        this.sonidosNumerosEnglish[9] = soundPool.load(getApplicationContext(),R.raw.nine,1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sobre) {
            // TODO: Comprobar si se puede definir el intent fuera del método para que no se cree cada vez que se llama
            // Cargamos la actividad sobre
            Intent intent = new Intent();
            // definimos las propiedades del intent
            intent.setClass(MainActivity.this, AboutActivity.class);
            // lo ejecutamos para pasar a la actividad destino
            startActivity(intent);
            //makeText(getApplicationContext(), "BOTON SOBRE", LENGTH_SHORT).show();
        } else if (id == R.id.action_salir) {
            // Cerramos la aplicación
            this.finishAffinity();
        }
        return super.onOptionsItemSelected(item);
    }
}
